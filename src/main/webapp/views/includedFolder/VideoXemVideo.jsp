<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<div class="most-popular">
	<div class="row">
		<div class="col-lg-12">
			<div class="heading-section">
				<h4>
					<em>${videoPlay.title}</em>
				</h4>
			</div>
			<div class="container-fluid">
				<div class="row">
					<!-- YouTube video section -->
					<div class="col-md-9">
						<div class="embed-responsive embed-responsive-16by9 mb-3">
							<iframe src="${videoPlay.videoLink }"
								style="width: 100%; height: 460px" title="Vimeo video"
								allowfullscreen></iframe>
						</div>
						<div class="row bg-dark">
						<div class="col-md-11">
						<h1 class="h4 mb-3 text-white">${videoPlay.title}</h1>
						<h1 class="h4 mb-3 text-white">Views ${videoPlay.views}</h1>
						</div>
						<a href="#" class="col-md-1 bg-dark"><img alt="" src="https://th.bing.com/th/id/OIP.WEDIaM0LIbpI9pMyRp6e-gHaHa?pid=ImgDet&rs=1"></a>
						</div>
						
						<p class="mb-3 text-white text-white">${videoPlay.description}</p>
					</div>
					<!-- Aside bar section -->
					<div class="col-md-3">
						<div class="card mb-3 bg-dark">
							<div class="card-header text-white">Video Liên Quan</div>
							<table class="table table-dark">
								<c:forEach var="video" items="${listVideo}">
									<tr>
										<div class="col-lg-12 col-sm-6">
											<div class="item">
												<a
													href="${pageContext.request.contextPath}/user/XemVideoServlet/?id=${video.id }">
													<img
													src="https://th.bing.com/th/id/OIP.kEmoEOd1QuwG7usl9LmsQgHaE8?pid=ImgDet&rs=1"
													alt="">
												</a>
												<h4>
													<a href="index.html" class="logo"> <img
														src="assets/android-chrome-192x192.png" alt=""
														class="rounded-circle shadow-4-strong"
														style="width: 30px; height: 3-px">
													</a> Admin<br> <span class="text-white">${video.title}</span>
												</h4>
											</div>
										</div>
									</tr>
								</c:forEach>
							</table>


						</div>
						<div class="card mb-3">
							<div class="card-header">About the Channel</div>
							<div class="card-body">
								<p>{CHANNEL_DESCRIPTION}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Bootstrap 5 JS -->
			<script
				src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
				integrity="sha384-gJlzG0Q+vpT/29R1Y02otYaLJysmSjx03ZI+dcQzJlO7C4X4X+IIfCJIlzzL1PFp"
				crossorigin="anonymous"></script>
		</div>
	</div>
</div>