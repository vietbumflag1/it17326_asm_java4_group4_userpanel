<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>	

<div class="most-popular">
	<div class="row">
		<div class="col-lg-12">
			<div class="heading-section">
				<h4>
					<em>Video</em> Hàng Ngày
				</h4>
			</div>
			<div class="row">
				<c:forEach var="video" items="${listDailyVideo}">
					<div class="col-lg-3 col-sm-6">
						<div class="item pb-20">
							<a href="${pageContext.request.contextPath}/user/XemVideoServlet/?id=${video.id }"> <img
								src="https://cdn.dribbble.com/users/60166/screenshots/4506607/player.jpg" alt="">
							</a>
							<h4>
								<a class="logo"> <img
									src="assets/android-chrome-192x192.png" alt=""
									class="rounded-circle shadow-4-strong"
									style="width: 30px; height: 3-px">
								</a> Admin<br> <span class="text-white">${video.title}</span>
								<li>
									<h4>Lượt xem</h4> <span class="fw-bold text-white">${video.views}</span>
								</li>
							</h4>
							<ul>
								<li style="font-size: 20px"><i class="fa fa-star"></i> 1</li>
								<li style="font-size: 20px"><i
									class="fa-solid fa-paper-plane"></i> 2</li>
								<li style="font-size: 20px"><i class="fa fa-download"></i>
									23</li>
							</ul>
							<li><span class="fw-bold text-white">Ngày đăng:
									${video.ngayDang}</span></li>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>