<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<div class="most-popular">
	<div class="row">
		<div class="col-lg-12">
			<div class="heading-section">
				<h4>
					<em>Video Hot</em> Ngay và Bây giờ
				</h4>
			</div>
			<div class="row">
				<c:forEach var="video" items="${listVideo}">
					<div class="col-lg-3 col-sm-6">
						<div class="item pb-20">
							<a href="${pageContext.request.contextPath}/user/XemVideoServlet/?id=${video.id }"> <img
								src="https://th.bing.com/th/id/OIP.kEmoEOd1QuwG7usl9LmsQgHaE8?pid=ImgDet&rs=1" alt="">
							</a>
							<h4>
								<a class="logo"> <img
									src="assets/android-chrome-192x192.png" alt=""
									class="rounded-circle shadow-4-strong"
									style="width: 30px; height: 3-px">
								</a> Admin<br> <span class="text-white">${video.title}</span>
								<li>
									<h4>Lượt xem</h4> <span class="fw-bold text-white">${video.views}</span>
								</li>
							</h4>
							<ul>
								<li style="font-size: 20px"><i class="fa fa-star"></i> 1</li>
								<li style="font-size: 20px"><i
									class="fa-solid fa-paper-plane"></i> 2</li>
								<li style="font-size: 20px"><i class="fa fa-download"></i>
									23</li>
							</ul>
							<li><span class="fw-bold text-white">Ngày đăng:
									${video.ngayDang}</span></li>
						</div>
					</div>
				</c:forEach>
				<%-- <c:forEach var="video" items="${listVideo}">
					<p>${video.id}</p>
					<p>${video.title}</p>
					

				</c:forEach> --%>
			</div>
		</div>
	</div>
</div>