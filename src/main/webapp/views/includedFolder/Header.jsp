<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<header class="header-area header-sticky">


	<div class="container">
		<div class="row">
			<div class="col-12">
				<nav class="main-nav">
					<!-- ***** Logo Start ***** -->
					<a href="index.html" class="logo"> <img
						src="https://th.bing.com/th/id/OIP.npJwKrNPGnk4L7QbR18WaQHaFw?pid=ImgDet&rs=1" alt=""
						class="rounded-circle shadow-4-strong"
						style="width: 50px; height: 50px">
					</a>
					<!-- ***** Logo End ***** -->
					<!-- ***** Search End ***** -->
					<!-- <div class="search-input">
						<form id="search" action="#">
							<input type="text" placeholder="Type Something" id='searchText'
								name="searchKeyword" onkeypress="handle" /> <i
								class="fa fa-search"></i>
						</form>
					</div> -->
					<!-- ***** Search End ***** -->
					<!-- ***** Menu Start ***** -->
					<ul class="nav">
						<li><a href="IndexServlet"><h4>Trang chủ</h4></a></li>
						<li><a href="YeuThichServlet"><h4>Yêu Thích</h4></a></li>
						<li><a href="ThuVienServlet"><h4>Thư Viện</h4></a></li>
						<li><a href="PhimDaMuaServlet"><h4>Phim đã mua</h4></a></li>
						<li><a href="ProfileServlet"><img src="https://th.bing.com/th/id/OIP.G70nGHSg94MMxYQD8vZL8gHaHa?pid=ImgDet&rs=1" alt=""></a></li>
					</ul>
					<a class='menu-trigger'> <span>Menu</span>
					</a>
					<!-- ***** Menu End ***** -->
				</nav>
			</div>
		</div>
	</div>
</header>
<!-- ***** Header Area End ***** -->