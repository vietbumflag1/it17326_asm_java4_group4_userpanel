<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Thư viện</title>
    <link rel="shortcut icon" href="assets/favicon.ico" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
	<script src="https://kit.fontawesome.com/05e6f0c772.js"></script>
	
    <style type="text/css">
  		<jsp:include page="/views/assets/css/fontawesome.css" />
  		<jsp:include page="/views/assets/css/templatemo-cyborg-gaming.css" />
  		<jsp:include page="/views/assets/css/owl.css" />
  		<jsp:include page="/views/assets/css/animate.css" />
  		
  		<jsp:include page="/views/vendor/bootstrap/css/bootstrap.min.css" />
	</style>
  </head>

<body>

  <!-- ***** Header Area Start ***** -->
  <jsp:include page="/views/includedFolder/Header.jsp" />

  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="page-content">

          <!-- ***** Most Popular Start ***** -->
  			<jsp:include page="/views/includedFolder/VideoThuVien.jsp" />
          <!-- ***** Most Popular End ***** -->

        </div>
      </div>
    </div>
  </div>
  
  <jsp:include page="/views/includedFolder/Footer.jsp" />
  
 </body>

</html>