package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import DAO.VideoDao;

/**
 * Servlet implementation class XemVideoServlet
 */
@WebServlet({"/XemVideoServlet/*","/user/XemVideoServlet/*"})
public class XemVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public XemVideoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		findVideoOnClick(request, response, id);
		findAll(request, response);
		request.getRequestDispatcher("/views/XemVideo.jsp").forward(request, response);
	}
	protected void findAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			VideoDao dao = new VideoDao();
			request.setAttribute("listVideo", dao.findAll());
		}
		catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("error", "Error: "+e.getMessage());
		}
	}
	protected void findVideoOnClick(HttpServletRequest request, HttpServletResponse response,int id) throws ServletException, IOException {
		try {
			VideoDao dao = new VideoDao();
			request.setAttribute("videoPlay", dao.findById(id));
			dao.updateVideoViews(id);
			System.out.println(id);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
