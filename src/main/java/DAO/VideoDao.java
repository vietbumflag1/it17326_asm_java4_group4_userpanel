package DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.hibernate.sql.Insert;

import Ultis.JpaUtils;
import model.Video;

public class VideoDao extends ConnectDao {
	private EntityManager em = JpaUtils.getEntityManager();

	protected void finalize() throws Throwable {
		em.close();
		super.finalize();
	}

	public Video create(Video entity) {
		try {
			em.getTransaction().begin();

			em.persist(entity);// them

			em.getTransaction().commit();
			return entity;
		} catch (Exception e) {

			em.getTransaction().rollback();
			throw new RuntimeException(e);
			// TODO: handle exception
		}
	}

	// lấy hết video lên
	public List<Video> findAll() {
		String jpql = "Select o from Video o";
		TypedQuery<Video> query = em.createQuery(jpql, Video.class);
		return query.getResultList();
	}
	public List<Video> findbyDate(){
		String jpql = "Select o from Video o ORDER BY o.ngayDang DESC";
		TypedQuery<Video> query = em.createQuery(jpql, Video.class);
		return query.getResultList();
	}

	public Video findById(int id) {
		return em.find(Video.class, id);
	}

	public Video remove(int id) {
		try {
			em.getTransaction().begin();
			Video entity = this.findById(id);
			em.remove(entity);
			em.getTransaction().commit();
			return entity;
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new RuntimeException(e);
			// TODO: handle exception
		}
	}
	public Video updateVideoViews(int videoId) {
	    try {
	    	Video video = em.find(Video.class, videoId);
	        video.setViews(video.getViews() + 1);
	        em.merge(video);
	        return video;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
