package model;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * The persistent class for the Favorite database table.
 * 
 */
@Entity
@NamedQuery(name="Favorite.findAll", query="SELECT f FROM Favorite f")
public class Favorite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Id")
	private int id;

	@Column(name="LikeDate")
	private Date likeDate;
	
	@Column(name="UserId")
	private int userid;
	
	@Column(name="VideoId")
	private int videoid;

	

	public Favorite() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getLikeDate() {
		return likeDate;
	}

	public void setLikeDate(Date likeDate) {
		this.likeDate = likeDate;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getVideoid() {
		return videoid;
	}

	public void setVideoid(int videoid) {
		this.videoid = videoid;
	}

	public Favorite(int id, Date likeDate, int userid, int videoid) {
		super();
		this.id = id;
		this.likeDate = likeDate;
		this.userid = userid;
		this.videoid = videoid;
	}

	

}