package model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;


/**
 * The persistent class for the Download database table.
 * 
 */
@Entity
@NamedQuery(name="Download.findAll", query="SELECT d FROM Download d")
public class Download implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="Id")
	private int id;

	@Column(name="DownloadDate")
	private Date downloadDate;

	@Column(name="UserId")
	private int userid;
	
	@Column(name="VideoId")
	private int videoid;

	

	public Download() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Download(int id, Date downloadDate, int userid, int videoid) {
		super();
		this.id = id;
		this.downloadDate = downloadDate;
		this.userid = userid;
		this.videoid = videoid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDownloadDate() {
		return downloadDate;
	}

	public void setDownloadDate(Date downloadDate) {
		this.downloadDate = downloadDate;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getVideoid() {
		return videoid;
	}

	public void setVideoid(int videoid) {
		this.videoid = videoid;
	}

	
}