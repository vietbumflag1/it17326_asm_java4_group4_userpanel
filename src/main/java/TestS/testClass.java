package TestS;

import java.util.List;

import DAO.FavoriteDAO;
import DAO.UsersDAO;
import DAO.VideoDao;
import model.Favorite;
import model.User;
import model.Video;

public class testClass {
	public static void main(String[] args) {
		testLogin();
		
	}
	public static void testVideo() {
		List<Video> vd = new VideoDao().findAll();
		for (Video video : vd) {
			System.out.print(video.getId());
			System.out.print(video.getTitle());
			System.out.println(video.getViews());
		}
		Video vdid = new VideoDao().findById(1);
		System.out.println(vdid.getId());
	}
	public static void testFavorite() {
		List<Favorite> fvt = new FavoriteDAO().findAll();
		for (Favorite favorite : fvt) {
			System.out.println(favorite.getId());
		}
	}
	public static void testLogin() {
		String email = "nguyenthanhnam@gmail.com";
		String pass = "123456";
		User us = new UsersDAO().getUserByEmailAndPassword(email, pass);
	}
	public static void testViews() {
		Video vd = new VideoDao().updateVideoViews(2);
		System.out.println(vd.getViews());
	}
}
